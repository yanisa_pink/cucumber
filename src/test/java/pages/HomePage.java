package pages;

import Base.BaseUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Karthik on 10/25/2016.
 */
public class HomePage {

    private BaseUtil base;

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.XPATH, using = "//button[@class='sim-btn -primary']")
    public WebElement btnLogin;

    public WebDriverWait wait = new WebDriverWait(base.Driver, 5);

    public void CheckPopup(WebDriver driver)
    {
        try {
            driver.switchTo().frame(driver.findElement(By.cssSelector( ".sp-fancybox-iframe" )));
            driver.findElement(By.cssSelector(".element-close-button")).click();
            System.out.println(" - Close pop up");
        } catch (WebDriverException e){
            System.out.println(" - No pop up");
        }

        driver.switchTo().defaultContent();
    }

    public void ClickLogin()
    {
        btnLogin.click();
    }

    public void VerifyLogin()
    {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='noti-box']")));
    }

    public void ClickLogout(WebDriver driver)
    {
        Actions action = new Actions(driver);
        WebElement profile = driver.findElement(By.xpath("//*[@class='noti-box']"));
        WebElement logout = driver.findElement(By.xpath("//div[@class='noti-head' and text()='ออกจากระบบ']"));
        action.moveToElement(profile);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='noti-head' and text()='ออกจากระบบ']")));
        action.moveToElement(logout).click().build().perform();
    }

    public void VerifyLogout()
    {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='sim-btn -primary']")));
    }

}
