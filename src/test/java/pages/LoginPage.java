package pages;

import Base.BaseUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Karthik on 10/25/2016.
 */
public class LoginPage {

    private BaseUtil base;

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, using = "bt_signin")
    public WebElement btnSignin;

    public WebDriverWait wait = new WebDriverWait(base.Driver, 5);

    public void Login(WebDriver driver, String userName, String password) throws InterruptedException
    {
        driver.switchTo().frame(driver.findElement( By.cssSelector( ".fancybox-iframe" ) ));
        driver.findElement(By.name("account")).sendKeys(userName);
        driver.findElement(By.name("password")).sendKeys(password);
        driver.switchTo().defaultContent();
    }

    public void ClickSignin(WebDriver driver)
    {
        driver.switchTo().frame(driver.findElement( By.cssSelector( ".fancybox-iframe" ) ));
        btnSignin.submit();
        driver.switchTo().defaultContent();
    }

    public void AlertAccountError()
    {
        base.Driver.switchTo().frame(base.Driver.findElement( By.cssSelector( ".fancybox-iframe" ) ));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("signin-account-error-left")));
        base.Driver.switchTo().defaultContent();
    }

}
