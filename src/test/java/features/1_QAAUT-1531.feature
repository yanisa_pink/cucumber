Feature: Login - username and password incorrect

	Background:
		#@PRECOND_QAAUT-1533
		Given I navigate to the home page

	#Xray description
	@TEST_QAAUT-1531 @TESTSET_QAAUT-1532
	Scenario: Login - username and password incorrect
		When I click login button
		And I enter tests and 1234
		And I click signin button
		Then I should see alert username and password incorrect
