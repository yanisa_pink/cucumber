Feature: Login - username and password correct

    Background:
        #@PRECOND_QAAUT-1533
        Given I navigate to the home page

	#Xray description
    @TEST_QAAUT-1292 @TESTSET_QAAUT-1532
    Scenario: Login - username and password correct
        When I click login button
        And I enter tests2@test.com and 1234testtest
        And I click signin button
        Then I should see the profile
