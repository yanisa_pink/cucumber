package steps;

import Base.BaseUtil;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import pages.HomePage;

public class LogoutStep extends BaseUtil{

    private  BaseUtil base;
    private  String urlTrueID = "https://home.trueid.net/";
    private  HomePage homePage = new HomePage(base.Driver);

    public LogoutStep(BaseUtil base) {
        this.base = base;
    }

    @And("^I click logout button$")
    public void iClickLogoutButton() {
        System.out.println("Click logout");
        homePage.ClickLogout(base.Driver);
    }

    @Then("^I should see the login button$")
    public void iShouldSeeTheLoginButton() {
        System.out.println("Verify logout");
        homePage.VerifyLogout();
    }

}
