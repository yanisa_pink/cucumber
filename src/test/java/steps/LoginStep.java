package steps;

import Base.BaseUtil;
import cucumber.api.DataTable;
import cucumber.api.Transform;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.HomePage;
import pages.LoginPage;
import transform.ExcelDataToDataTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karthik on 31/01/2019.
 */
public class LoginStep extends BaseUtil{

    private  BaseUtil base;
    private  String urlTrueID = "https://home.trueid.net/";
    private  HomePage homePage = new HomePage(base.Driver);
    private  LoginPage loginPage = new LoginPage(base.Driver);

    public LoginStep(BaseUtil base) {
        this.base = base;
    }

    @Then("^I should see the profile$")
    public void iShouldSeeTheUserformPage() throws Throwable {
        System.out.println("Verify login");
        homePage.VerifyLogin();
    }

    @Given("^I navigate to the home page$")
    public void iNavigateToTheLoginPage() throws Throwable {
        System.out.println("Navigate home Page");
        base.Driver.navigate().to(urlTrueID);
        Thread.sleep(5000);
    }


    @When("^I click login button$")
    public void iClickLoginButton() throws Throwable {
        System.out.println("Check Pop up TruePoint");
        homePage.CheckPopup(base.Driver);
        System.out.println("Click login button");
        homePage.ClickLogin();
    }


    @And("^I input username and password with excel \"([^\"]*)\"$")
    public void iEnterTheFollowingForLogin(@Transform(ExcelDataToDataTable.class) DataTable table) throws Throwable {
        List<User> userList = table.asList(User.class);

        for(User user : userList){
            loginPage.Login(base.Driver, user.username, user.password);
        }
    }

    @And("^I click signin button$")
    public void iClickSigninButton() throws Throwable {
        System.out.println("Click sign in");
        loginPage.ClickSignin(base.Driver);
    }

    @And("^I enter ([^\"]*) and ([^\"]*)$")
    public void iEnterUsernameAndPassword(String username, String password) throws Throwable {
        loginPage.Login(base.Driver, username, password);
    }

    @Then("^I should see alert username and password incorrect$")
    public void iShouldSeeAlertUsernameAndPasswordIncorrect() {
        loginPage.AlertAccountError();
    }


    public class User {
        public User(String username,String password){
            this.username = username;
            this.password = password;
        }

        private String username;
        private String password;

        public String toString() {
            return String.format("Username = %s Password = %s", this.username, this.password);
        }
    }

}
